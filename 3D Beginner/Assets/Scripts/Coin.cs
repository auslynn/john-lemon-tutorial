﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public GameObject coin;
    public GameEnding gameEnding;
    // Update is called once per frame
    void Update()
    {
        coin.transform.Rotate(new Vector3(0, 0, 90) * Time.deltaTime);
    }
/*
    void onTriggerEnter(Collider other)
    {
        if (other.name == "JohnLemon")
        {
            Debug.Log("John touched the coin");
            coin.SetActive(false);
            gameEnding.coinCount += 1;
        }
    }
    */
}
