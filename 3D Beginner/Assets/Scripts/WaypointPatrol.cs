﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;

    int m_CurrentWaypointIndex = 0;

    void Start ()
    {
        Debug.Log("started movement");
        navMeshAgent.SetDestination (waypoints[0].position);
    }

    void Update ()
    {
        //Debug.Log("update");
        if(navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination (waypoints[m_CurrentWaypointIndex].position);
        }
        
        /*
        if(navMeshAgent.transform == waypoints[m_CurrentWaypointIndex].transform)
        {
            Debug.Log("should stop");
            if(m_CurrentWaypointIndex < waypoints.Length)
            {
                Debug.Log("works" + name + m_CurrentWaypointIndex);
                m_CurrentWaypointIndex += 1;
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
            else
            {
                Debug.Log("reset" + name + m_CurrentWaypointIndex);
                m_CurrentWaypointIndex = 0;
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
        }
        */
    }
}