﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameEnding gameEnding;
    public float moveSpeed = 5f;
    public float turnSpeed = 20f;
    public GameObject tp1;
    public GameObject tp2;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Vector3 m_walkMovement;
    Vector3 tempMovement;
    Quaternion m_Rotation = Quaternion.identity;
    bool canTeleport1 = true;
    bool canTeleport2 = true;
    

    void Start ()
    {
        m_Animator = GetComponent<Animator> ();
        m_Rigidbody = GetComponent<Rigidbody> ();
        m_AudioSource = GetComponent<AudioSource> ();
        //Vector3 m_walkMovement = m_Movement * moveSpeed;
    }

    void FixedUpdate ()
    {
        float horizontal = Input.GetAxis ("Horizontal");
        float vertical = Input.GetAxis ("Vertical");
        
        m_Movement.Set(horizontal, 0f, vertical);
       // m_walkMovement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize ();

        bool hasHorizontalInput = !Mathf.Approximately (horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately (vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool ("IsWalking", isWalking);
        
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop ();
        }

        Vector3 desiredForward = Vector3.RotateTowards (transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation (desiredForward);

        if(Input.GetButtonDown("Submit"))
        {
            
            m_Rigidbody.AddForce(m_Movement * 5f, ForceMode.Impulse);
        }
        if(Input.GetButtonUp("Submit"))
        {
            m_Movement = new Vector3(0, 0, 0);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Coin")
        {
            Debug.Log(gameEnding.coinCount);
            other.gameObject.SetActive(false);
            gameEnding.coinCount += 1;
            Debug.Log(gameEnding.coinCount);
        }
        else if(other.name == "Teleporter" && canTeleport1 == true)
        {
            Debug.Log("teleporter 1 entered");
            transform.position = tp2.transform.position;
            canTeleport2 = false;
        }
        else if(other.name == "Teleporter (1)" && canTeleport2 == true)
        {
            Debug.Log("teleporter 2 entered");
            transform.position = tp1.transform.position;
            canTeleport1 = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.name == "Teleporter")
        {
            canTeleport1 = true;
        }
        else if(other.name == "Teleporter (1)")
        {
            canTeleport2 = true;
        }
    }

    void OnAnimatorMove ()
    {
        m_Rigidbody.MovePosition (m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation (m_Rotation);
    }
}